/*
 * File Name: Spectre.cpp
 * Author: Pkamara
 * Description: Main interface file
 */

#include "Rendering\RenderWindow.h"

int main(int argc, char** argv)
{
	SDL_Init(SDL_INIT_VIDEO);
	Spectre::RenderWindow *window = new Spectre::RenderWindow();
	

	window->Update();

	Sleep(1000);

	return 1;
}