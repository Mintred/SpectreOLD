/*
 * File Name: Vector2.h
 * Author: Pkamara
 * Description: Standard 2-dimensional vector. Defined by X (Width) and Y (Height)
 */

#pragma once

namespace Spectre
{
	class Vector2
	{
		public:
			float X, Y;
		public:
			/*
			Default constructor
			*/
			inline Vector2() {}

			/*
			Manual constructor, you enter the 3 values yourself
			*/
			inline Vector2(const float nX, const float nY) :
				X(nX), Y(nY) {}

			inline Vector2& operator = (const Vector2& chkVector)
			{
				X = chkVector.X;
				Y = chkVector.Y;

				return *this;
			}

			inline bool operator == (const Vector2& chkVector) const
			{
				return (X == chkVector.X && Y == chkVector.Y);
			}

			inline bool operator != (const Vector2& chkVector) const
			{
				return (X != chkVector.X || Y != chkVector.Y);
			}

			inline Vector2 operator + (const Vector2& chkVector) const
			{
				return Vector2(
					X + chkVector.X,
					Y + chkVector.Y);
			}

			inline Vector2 operator - (const Vector2& chkVector) const
			{
				return Vector2(
					X - chkVector.X,
					Y - chkVector.Y);
			}

			inline Vector2 operator * (const Vector2& chkVector) const
			{
				return Vector2(
					X * chkVector.X,
					Y * chkVector.Y);
			}

			inline Vector2 operator / (const Vector2& chkVector) const
			{
				return Vector2(
					X / chkVector.X,
					Y / chkVector.Y);
			}

			inline Vector2 operator + () const
			{
				return *this;
			}

			inline Vector2 operator - () const
			{
				return Vector2(-X, -Y);
			}

			inline Vector2& operator += (const Vector2& chkVector)
			{
				X += chkVector.X;
				Y += chkVector.Y;
				return *this;
			}

			inline Vector2& operator -= (const Vector2& chkVector)
			{
				X -= chkVector.X;
				Y -= chkVector.Y;
				return *this;
			}

			inline Vector2& operator *= (const Vector2& chkVector)
			{
				X *= chkVector.X;
				Y *= chkVector.Y;
				return *this;
			}

			inline Vector2& operator /= (const Vector2& chkVector)
			{
				X /= chkVector.X;
				Y /= chkVector.Y;
				return *this;
			}
	};
}