/*
 * File Name: Vector3.h
 * Author: Pkamara
 * Description: Standard 3-dimensional vector. Defined by X (Width) , Y (Height) and Z (Depth)
 */

#pragma once

namespace Spectre
{
	class Vector3
	{
		public:
			float X, Y, Z;
		public:
			/*
				Default constructor
			*/
			inline Vector3() {}

			/*
				Manual constructor, you enter the 3 values yourself
			*/
			inline Vector3(const float nX, const float nY, const float nZ) :
				X(nX), Y(nY), Z(nZ) {}

			/* Operators */

			inline Vector3& operator = (const Vector3& chkVector)
			{
				X = chkVector.X;
				Y = chkVector.Y;
				Z = chkVector.Z;

				return *this;
			}

			inline bool operator == (const Vector3& chkVector) const
			{
				return (X == chkVector.X && Y == chkVector.Y && Z == chkVector.Z);
			}

			inline bool operator != (const Vector3& chkVector) const
			{
				return (X != chkVector.X || Y != chkVector.Y || Z != chkVector.Z);
			}

			inline Vector3 operator + (const Vector3& chkVector) const
			{
				return Vector3(
					X + chkVector.X,
					Y + chkVector.Y,
					Z + chkVector.Z);
			}

			inline Vector3 operator - (const Vector3& chkVector) const
			{
				return Vector3(
					X - chkVector.X,
					Y - chkVector.Y,
					Z - chkVector.Z);
			}

			inline Vector3 operator * (const Vector3& chkVector) const
			{
				return Vector3(
					X * chkVector.X,
					Y * chkVector.Y,
					Z * chkVector.Z);
			}

			inline Vector3 operator / (const Vector3& chkVector) const
			{
				return Vector3(
					X / chkVector.X,
					Y / chkVector.Y,
					Z / chkVector.Z);
			}

			inline Vector3 operator + () const
			{
				return *this;
			}

			inline Vector3 operator - () const
			{
				return Vector3(-X, -Y, -Z);
			}

			inline Vector3& operator += (const Vector3& chkVector)
			{
				X += chkVector.X;
				Y += chkVector.Y;
				Z += chkVector.Z;
				return *this;
			}

			inline Vector3& operator -= (const Vector3& chkVector)
			{
				X -= chkVector.X;
				Y -= chkVector.Y;
				Z -= chkVector.Z;
				return *this;
			}

			inline Vector3& operator *= (const Vector3& chkVector)
			{
				X *= chkVector.X;
				Y *= chkVector.Y;
				Z *= chkVector.Z;
				return *this;
			}

			inline Vector3& operator /= (const Vector3& chkVector)
			{
				X /= chkVector.X;
				Y /= chkVector.Y;
				Z /= chkVector.Z;
				return *this;
			}

			inline float Length () const
			{
				return Math::SquareRoot(X * X + Y * Y + Z * Z);
			}
	};
}
