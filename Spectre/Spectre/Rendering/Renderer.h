/*
 * File Name: Renderer.h
 * Author: Pkamara
 * Description: Allows for managing and choosing different render systems
 */

#pragma once

#include "../Spectre.h"
#include <vulkan.h>

#ifdef _WIN32
	#include <d3d11.h>
#endif

namespace Spectre
{
	enum RENDERER
	{
		ID_OPENGL,
		ID_VULKAN,
		ID_DX12,
		ID_DX11
	};
}