/*
 * File Name: MeshManager.h
 * Author: Pkamara
 * Description: Handles meshes internally
 */

#pragma once

#include "../Spectre.h"

namespace Spectre
{
	class MeshNode;

	class MeshManager
	{
		/*
			MeshManager Constructor
		*/
		MeshManager();

		/*
			MeshManager Deconstructor
		*/
		virtual ~MeshManager();
	};
}