/*
 * File Name: RenderWindow.h
 * Author: Pkamara
 * Description: The render window class
 */

#pragma once

#include "../Spectre.h"
#include "Renderer.h"

namespace Spectre
{
	enum SPEC_WINDOW_TYPE
	{
		// A windowed program
		SPEC_WINDOWED,
		// A full screen window
		SPEC_FULLSCREEN,
		// A maximized window 
		SPEC_MAXIMIZED,
	};

	struct RenderWindowSettings
	{
		std::string WindowTitle;
		RENDERER Renderer;
		int IntitalX;
		int InitialY;
		int WindowWidth;
		int WindowHeight;
	};

	class RenderWindow
	{
		public:
			/*
				Render Window (no arguments)
				The base class for rendering and handles window creation and destruction
			*/
			RenderWindow();

			/*
				Render Window (Settings)
				The base class for rendering and handles window creation and destruction
				and this window requires a Settings struct to be passed
			*/
			RenderWindow(RenderWindowSettings Settings);

			/*
				Deconstructor
			*/
			virtual ~RenderWindow();

			/*
				Destroys the window context
			*/
			void Destroy();

			/*
				Refreshes the window
			*/

			void Update();

			/*
				Returns the windows GLContext
				Should only be used if you know what you're doing
			*/
			SDL_GLContext GetGLContext();

			/*
				Returns a basic SDL_Event
			*/
			SDL_Event GetEvent();

			/*
				Sets the size of the active window
			*/
			void SetSize(int Width, int Height);

			/*
				Sets the position of the active window
			*/
			void SetPosition(int X, int Y);

			/*
				Changes the windows visibility state
			*/
			void SetVisible(bool Visible);

			/*
				Changes the state of the window to the enum passed
			*/
			void SetWindowState(SPEC_WINDOW_TYPE Type);
		private:
			// IsVisible
			bool Visible;
			// Render Window
			SDL_Window *internalWindow;
			// The GL context
			SDL_GLContext internalGlContext;
			// Event
			SDL_Event Event;
	};
}