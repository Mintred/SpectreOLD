/*
 * File Name: RenderWindow.h
 * Author: Pkamara
 * Description: The render window class
 */

#include "RenderWindow.h"

namespace Spectre
{
	RenderWindow::RenderWindow()
	{
		internalWindow = SDL_CreateWindow("Basic Render Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
		internalGlContext = SDL_GL_CreateContext(internalWindow);
		Visible = true;
	}

	RenderWindow::RenderWindow(RenderWindowSettings Settings)
	{
		internalWindow = SDL_CreateWindow(Settings.WindowTitle.c_str(), Settings.IntitalX, Settings.InitialY, Settings.WindowWidth, Settings.WindowHeight, SDL_WINDOW_OPENGL);
		internalGlContext = SDL_GL_CreateContext(internalWindow);
	}

	RenderWindow::~RenderWindow() 
	{
		Destroy();
	}

	void RenderWindow::Destroy()
	{
		SDL_GL_DeleteContext(internalGlContext);
		SDL_DestroyWindow(internalWindow);
	}

	void RenderWindow::Update()
	{
		SDL_GL_SwapWindow(internalWindow);
	}

	SDL_GLContext RenderWindow::GetGLContext()
	{
		return internalGlContext;
	}

	SDL_Event RenderWindow::GetEvent()
	{
		return Event;
	}

	void RenderWindow::SetSize(int Width, int Height)
	{
		SDL_SetWindowSize(internalWindow, Width, Height);
	}

	void RenderWindow::SetVisible(bool Visible)
	{
		if (Visible == true)
		{
			SDL_ShowWindow(internalWindow);
			this->Visible = true;
		}
		else if (Visible == false)
		{
			SDL_HideWindow(internalWindow);
			this->Visible = false;
		}
		else
		{
			throw new std::exception("tried to pass invalid argument to Visible");
		}
	}

	void RenderWindow::SetWindowState(SPEC_WINDOW_TYPE Type)
	{
		switch (Type)
		{
			case SPEC_WINDOWED:
				SDL_SetWindowFullscreen(internalWindow, 0);
				break;
			case SPEC_FULLSCREEN:
				SDL_SetWindowFullscreen(internalWindow, SDL_WINDOW_FULLSCREEN);
				break;
			case SPEC_MAXIMIZED:
				SDL_SetWindowFullscreen(internalWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
				break;
		}
	}

	void RenderWindow::SetPosition(int X, int Y)
	{
		SDL_SetWindowPosition(internalWindow, X, Y);
	}
}