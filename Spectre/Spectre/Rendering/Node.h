/*
 * File Name: Node.h
 * Author: Pkamara
 * Description: An object that can accessed through
 */

#pragma once

#include "../Dependencies.h"

namespace Spectre
{
	class Node
	{
		public:
			/*
				Node Constructor
			*/
			Node();

			/*
				Node Deconstructor
			*/
			virtual ~Node();
		protected:
			Vector3 Position;
			Vector3 Scale;
	};
}