/*
 * File Name: Renderer.h
 * Author: Pkamara
 * Description: Platforms of which we support
 */

#pragma once

namespace Spectre
{
	/*
		Enums of the platforms we're planning on 
	*/
	enum Platform
	{
		PT_WINDOWS,
		PT_WINDOWS64,
		PT_LINUX,
		PT_MAC,
		PT_XBOX,
		PT_PLAYSTATION,
		PT_ANDRIOD
	};

	Platform GetCurrentPlatform()
	{
		#ifdef __unix__
			return Platform::PT_LINUX;
		#elif __APPLE__
			/* Allows us to check versions in the future more precisely */
			#include "AvailabilityMacros.h"
			#include "TargetConditionals.h"
			return Platform::PT_MAC;
		#elif _WIN32
			return Platform::PT_WINDOWS;
		#elif _WIN64
			return Platform::PT_WINDOWS64;
		#elif __MINGW32__
			return Platform::PT_WINDOWS32;
		#elif __MINGW64__
			reeturn Platform::WINDOWS64;
		#elif defined(ANDROID) || defined(__ANDROID__)
			return Platform::PT_ANDROID;
		#endif
	}
}
