/*
 * File Name: Debug.h
 * Author: Pkamara
 * Description: The debug handler for the engine
 */

#pragma once

namespace Spectre
{
	enum DEBUG_LEVEL
	{
		DBG_NONE,
		DBG_LOW,
		DBG_NORM,
		DBG_HIGH
	};
}
