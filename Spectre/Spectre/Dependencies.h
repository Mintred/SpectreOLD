/*
 * File Name: GLDeps.h
 * Author: Pkamara
 * Description: All the 3rd party dependencies required
 */

#pragma once

#include <GL\glew.h>
#include <SDL.h>
#include <chrono>
#include <iostream>
#include <vector>
#include <string>

/* Mathematical Helpers */

#include "Math.h"

#undef main