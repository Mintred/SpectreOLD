/*
 * File Name: Math.h
 * Author: Pkamara
 * Description: Basic class for mathematical calculations and so forth
 */

#pragma once

namespace Spectre
{
	namespace Math
	{
		static inline float SquareRoot(float Value) { return float(sqrt(Value)); }
	}
}