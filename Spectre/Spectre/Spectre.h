/*
 * File Name: Spectre.cpp
 * Author: Pkamara
 * Description: Main interface file
 */

#pragma once

#include "Dependencies.h"
#include "Rendering\RenderWindow.h"
#include "Debug.h"

namespace Spectre
{
	/*
		A container for all settings
	*/
	struct StateSettingsConstructor
	{
		DEBUG_LEVEL DebugLevel;
	};

	class Root
	{
		public:
			/*
				Root Default Constructor
			*/
			Root();

			/*
				Custom Root Constructor

				Allows for the user to create a state based off specific settings
			*/
			Root(StateSettingsConstructor Settings);

			/*
				Root Deconstructor
			*/
			virtual ~Root();
	};
}